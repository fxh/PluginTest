﻿using Abp.Application.Services;

namespace PluginTest
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class PluginTestAppServiceBase : ApplicationService
    {
        protected PluginTestAppServiceBase()
        {
            LocalizationSourceName = PluginTestConsts.LocalizationSourceName;
        }
    }
}