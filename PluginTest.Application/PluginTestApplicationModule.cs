﻿using System.Reflection;
using Abp.Modules;

namespace PluginTest
{
    [DependsOn(typeof(PluginTestCoreModule))]
    public class PluginTestApplicationModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
