﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using PluginTest.PluginCore.Mvc.Routes;
using System.Web.Routing;
using Abp.Dependency;
using Castle.Core.Logging;

namespace PluginTest.Plugin.Hello.World {
    public class RouteProvider : IRouteProvider, ISingletonDependency {
        public void RegisterRoutes(RouteCollection routes)
        {
            var logger = IocManager.Instance.Resolve<ILogger>();
            logger.Info("注册路由 PluginTest.Plugin.Hello.World");
            routes.MapRoute("PluginTest.Plugin.Hello.World.Index",
                 "Plugins/HelloWorld/Index",
                 new { controller = "HelloWorld", action = "Index" },
                 new[] { "PluginTest.Plugin.Hello.World.Controllers" }
            );
        }
        public int Priority => 0;
    }
}
