﻿using System.Reflection;
using Abp.Modules;

namespace PluginTest.Plugin.Hello.World {
    public class PluginTestPluginHelloWorldModule : AbpModule {
        public override void Initialize() {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}