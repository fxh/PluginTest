﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Abp.Web.Mvc.Controllers;

namespace PluginTest.Plugin.Hello.World.Controllers {
    public class HelloWorldController : AbpController {
        public ActionResult Index() {
            return View("~/bin/Plugins/HelloWorld/Views/HelloWorld/Index.cshtml");
        }
    }
}
