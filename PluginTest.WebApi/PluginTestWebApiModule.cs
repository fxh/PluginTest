﻿using System.Reflection;
using Abp.Application.Services;
using Abp.Modules;
using Abp.WebApi;
using Abp.WebApi.Controllers.Dynamic.Builders;

namespace PluginTest
{
    [DependsOn(typeof(AbpWebApiModule), typeof(PluginTestApplicationModule))]
    public class PluginTestWebApiModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(PluginTestApplicationModule).Assembly, "app")
                .Build();
        }
    }
}
