﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace PluginTest.EntityFramework.Repositories
{
    public abstract class PluginTestRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<PluginTestDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected PluginTestRepositoryBase(IDbContextProvider<PluginTestDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class PluginTestRepositoryBase<TEntity> : PluginTestRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected PluginTestRepositoryBase(IDbContextProvider<PluginTestDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
