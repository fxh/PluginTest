﻿using System.Data.Entity;
using System.Reflection;
using Abp.EntityFramework;
using Abp.Modules;
using PluginTest.EntityFramework;

namespace PluginTest
{
    [DependsOn(typeof(AbpEntityFrameworkModule), typeof(PluginTestCoreModule))]
    public class PluginTestDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            Database.SetInitializer<PluginTestDbContext>(null);
        }
    }
}
