using System.Data.Entity.Migrations;

namespace PluginTest.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<PluginTest.EntityFramework.PluginTestDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "PluginTest";
        }

        protected override void Seed(PluginTest.EntityFramework.PluginTestDbContext context)
        {
            // This method will be called every time after migrating to the latest version.
            // You can add any seed data here...
        }
    }
}
