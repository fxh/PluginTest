﻿using System.Web.Mvc;

namespace PluginTest.Web.Controllers
{
    public class HomeController : PluginTestControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}