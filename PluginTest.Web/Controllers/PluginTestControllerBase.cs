﻿using Abp.Web.Mvc.Controllers;

namespace PluginTest.Web.Controllers
{
    /// <summary>
    /// Derive all Controllers from this class.
    /// </summary>
    public abstract class PluginTestControllerBase : AbpController
    {
        protected PluginTestControllerBase()
        {
            LocalizationSourceName = PluginTestConsts.LocalizationSourceName;
        }
    }
}