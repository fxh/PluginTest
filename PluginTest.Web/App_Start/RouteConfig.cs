﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Abp.Dependency;
using Castle.Core.Logging;
using PluginTest.PluginCore.Mvc.Routes;

namespace PluginTest.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes) {
            var logger = IocManager.Instance.Resolve<ILogger>();
            logger.Info(@"开始 RegisterRoutes");

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            var routePublisher = IocManager.Instance.Resolve<IRoutePublisher>();
            routePublisher.RegisterRoutes(routes);

            //ASP.NET Web API Route Config
            routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
