﻿using Abp.Web.Mvc.Views;

namespace PluginTest.Web.Views
{
    public abstract class PluginTestWebViewPageBase : PluginTestWebViewPageBase<dynamic>
    {

    }

    public abstract class PluginTestWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected PluginTestWebViewPageBase()
        {
            LocalizationSourceName = PluginTestConsts.LocalizationSourceName;
        }
    }
}