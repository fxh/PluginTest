﻿using System;
using Abp.Dependency;
using Abp.Web;
using Castle.Core.Logging;
using Castle.Facilities.Logging;

namespace PluginTest.Web
{
    public class MvcApplication : AbpWebApplication {
        protected override void Application_Start(object sender, EventArgs e)
        {
            IocManager.Instance.IocContainer.AddFacility<LoggingFacility>(f => f.UseLog4Net().WithConfig("log4net.config"));
            var logger = IocManager.Instance.Resolve<ILogger>();
            logger.Info("Application_Start 开始");

            WebAssemblyFinder.FindAssembliesSearchOption = System.IO.SearchOption.AllDirectories;


            base.Application_Start(sender, e);
        }
    }
}
