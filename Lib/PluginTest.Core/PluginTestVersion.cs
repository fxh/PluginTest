﻿namespace PluginTest.PluginCore {
    public class PluginTestVersion {
        /// <summary>
        /// Get version
        /// </summary>
        public static string CurrentVersion => "1.00";
    }
}
