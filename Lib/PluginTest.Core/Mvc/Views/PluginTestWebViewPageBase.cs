﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Web.Mvc.Views;

namespace PluginTest.PluginCore.Mvc.Views {
    public abstract class PluginTestWebViewPageBase : PluginTestWebViewPageBase<dynamic> {

    }

    public abstract class PluginTestWebViewPageBase<TModel> : AbpWebViewPage<TModel> {

    }
}
