﻿using System.Web.Routing;

namespace PluginTest.PluginCore.Mvc.Routes {
    public interface IRouteProvider {
        int Priority { get; }
        void RegisterRoutes(RouteCollection routes);
    }
}