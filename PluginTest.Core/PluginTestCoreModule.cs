﻿using System.Reflection;
using Abp.Modules;

namespace PluginTest
{
    public class PluginTestCoreModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
